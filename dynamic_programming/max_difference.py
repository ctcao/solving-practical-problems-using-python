#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Maximum difference between two elements where larger element appears after the 
smaller element

Given an array A[], write an algo­rithm to find Max­i­mum dif­fer­ence between two 
ele­ments where larger ele­ment appears after the smaller ele­ment or in other 
words find A[i] and A[j] such that A[j]-A[i] is max­i­mum where j > i.

int [] A = { 2, 5, 1, 7, 3, 9, 5};
Maximum Difference between two elements A[i] and A[j] and where j > i: 8

int [] A = { 22,2, 12, 5, 4, 7, 3, 19, 5};
Maximum Difference between two elements A[i] and A[j] and where j > i: 17

Created on Wed Nov  8 22:13:10 2017

@author: Lilun Cao
"""

def max_difference(A):
    if not A:
        raise ValueError("Empty array")
    
    a_size = len(A)
    max_element = A[a_size-1]
    max_diff = 0
    max_indx = a_size-1
    small_indx = 0
    temp_indx = a_size-1
    for i in range(a_size-2,-1,-1):
        if A[i] > max_element:
            max_element = A[i]
            temp_indx = i
        elif max_diff < max_element - A[i]:
            max_diff = max_element - A[i]
            small_indx = i
            max_indx = temp_indx
            
    return A[small_indx], A[max_indx], max_diff

if __name__ == '__main__':
    #A = [2,5,1,7,3,9,5]
    A = [22,2, 12, 5, 4, 7, 3, 19, 5]
    small, max, max_diff = max_difference(A)
    print("small_value = %d, max_value = %d, max_diff = %d" % (small,  max, max_diff))   

   