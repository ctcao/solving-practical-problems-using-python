#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Stock Single Sell Problem — O(n) Solution

Given an array rep­re­sents cost of a stock on each day. You are allowed to buy 
and sell the stock only once. Write an algo­rithm to max­i­mize the profit in 
sin­gle buy and sell.

Exam­ple:

int[] prices = {200, 500, 1000, 700, 30, 400, 900, 400, 50};

Output: Maximum Profit: 870, buy date index: 4, sell date index: 6

Created on Thu Nov  9 21:20:58 2017

@author: Lilun Cao
"""

def stocksinglesell(prices):
    if not prices:
        raise ValueError("No prices provided")
    nprices = len(prices)
    maxprice = prices[nprices-1]
    tempday = 0
    maxprofit = 0
    sellday = 0
    buyday = 0
    for n in range(nprices-2,-1,-1):
        if maxprice < prices[n]:
            maxprice = prices[n]
            tempday = n
        elif maxprofit < maxprice - prices[n]:
            maxprofit = maxprice - prices[n]
            sellday = tempday
            buyday = n
    return buyday, sellday, maxprofit

if __name__ == '__main__':
    prices = [200, 500, 1000, 700, 30, 400, 900, 400, 50]
    buyday, sellday, maxprofit = stocksinglesell(prices)
    print("Maximum Profit: %d, buy date index: %d, sell date index: %d" % (maxprofit, buyday, sellday))
