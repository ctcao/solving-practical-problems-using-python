# README #

This project demonstrates some algorithms for solving practical programming problems using python. Listed here are general sorting programming, data structure programming, recursive programming, dynamic programming, math programming and general programming. More will come.

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### General programming ###
* The Word Break Problem

### Sorting Algorithms ###
* Quick sort
* Merge sort
* Selection sort
* Bubble sort

### Data Structure ###
* Merge K Sorted Arrays
* Max revenue by windows


### Recursive Programming ###
* The Coin Change Problem
* Stairs Climbing Puzzle
* Print All Possible Subsets with Sum equal to a given Number
* Print all subsets of a set


### Dynamic Programming ###
* Maximum difference between two elements where larger element appears after the 
smaller element
* Stock Single Sell Problem — O(n) Solution
* Find longest Snake sequence in a given matrix
* Maximum Sum of All Sub-arrays 
* Longest Increasing subsequence
* Minimum Steps to One

### Math ###
* Find the sum of even-valued terms of the Fibonacci sequence
* Find the sum of all the multiples of 3 or 5 below 1000



### How do I get set up? ###

* Each program is relatively independent. So they can be used independently.


