#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
This program demos how to do the sorting of a list using bubble sorting algorithm
It has BigO(n^2) efficiency
Created on Sun Oct 22 14:58:23 2017

@author: Lilun Cao
"""

def bubble_sort(aList):
    """The input argument aList is a list of numbers. This function will return a 
    sorted list of numbers after the traditional bubble sorting algorithm is implemented.
    This algorithm is not efficient compared to other algorithms like merge sort.
    """
    if not aList:
        raise ValueError("aList can not be empty")
        
    try:
        if type(aList) != 'list':
            aList = list(aList)
            
        count = len(aList)
        for i in range(count):
            for j in range(count - i - 1):
                if aList[j] > aList[j+1]:
                    print("aList[%d] > aList[%d]" % (j, j+ 1))
                    aList[j], aList[j+1] = aList[j+1], aList[j]
                    
    except TypeError:
        raise TypeError("The input argument aList can not be converted to a list object")
        
    return aList

if __name__ == '__main__':
    a = [3, 6, 8 ,9 ,25,20, 59, 26]
    b = bubble_sort(a)
    print(b); print(a)
    c = (25,20)
    d = bubble_sort(c)
    print(d)